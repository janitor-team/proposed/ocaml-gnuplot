(* 	$Id: ex6.ml,v 1.3 2004-11-22 19:54:26 chris_77 Exp $	 *)
module P = Gnuplot
open Parse_args

let () =
  let re_sq u v = (u *. u -. v *. v, 2. *. u *. v, u) in
  let im_sq u v = (u *. u -. v *. v, 2. *. u *. v, v) in
  let g = P.init ~xsize:800. ~ysize:400. ~nxsub:2 (device 1)
            ?offline:(offline 1) in
  P.box3 g;
  P.pen g 1;
  P.fxy_param g re_sq (-3.) 3. (-3.) 3.;
  P.adv g;
  P.box3 g;
  P.fxy_param g im_sq (-3.) 3. (-3.) 3.;
  P.close g

let pi = 4. *. atan 1.

let () =
  let torus x0 y0 z0 r0 r1 u v =
    let r = r0 -. r1 *. cos(v) in
    (x0 +. r *. cos(u),  y0 +. r *. sin(u),  z0 +. r1 *. sin(v)) in
  let torus2 x0 y0 z0 r0 r1 u v =
    let r = r0 -. r1 *. cos(v) in
    (x0 +. r1 *. sin(v),  y0 +. r *. cos(u),  z0 +. r *. sin(u)) in
  let g = P.init ~xsize:800. ~ysize:400. ~nxsub:2 (device 2)
            ?offline:(offline 2) in
  P.box3 g;
  P.pen g 1;
  P.fxy_param g (torus 0. 0. 0. 1. 0.3) 0. (2.*.pi) 0. (2.*.pi);
  P.fxy_param g (torus 2. 0. 0. 1. 0.3) 0. (2.*.pi) 0. (2.*.pi);
  P.pen g 2;
  P.fxy_param g (torus2 0. 1. 0. 1. 0.2) 0. (2.*.pi) 0. (2.*.pi);
  P.adv g;
  let moebius u v =
    let r = 2. -. v *. sin(u/.2.) in
    (r *. sin(u),  r *. cos(u),  v *. cos(0.5 *. u)) in
  P.pen g 0;
  P.box3 g;
  P.pen g 1;
  P.fxy_param g moebius 0. (2. *. pi) (-0.25) 0.25;
  P.close g
