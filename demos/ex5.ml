(* 	$Id: ex5.ml,v 1.2 2004-11-22 19:54:26 chris_77 Exp $	 *)

module P = Gnuplot
open Parse_args

let () =
  let f u v =
    let r = sqrt(u *. u +. v *. v) in sin(r) /. r in
  let g = P.init ?offline:(offline 1) (device 1) in
  P.box3 g;
  P.fxy g f (-10.) 10. (-10.) 10.;
  P.close g

let () =
  let sq t = t *. t in
  let f x y =
    let x2 = x *. x
    and y2 = y *. y in
    3. *. sq(1. -. x) *. exp(-. x2 -. sq(y +. 1.)) -.
      10. *. (x/.5. -. x**3. -. y**5.) *. exp(-. x2 -. y2) -.
      exp(-. sq(x+.1.) -. y2) /. 3. in
  let g = P.init ?offline:(offline 2) (device 2) in
  P.box3 g;
  P.fxy g f (-2.5) 2.5 (-2.5) 2.5;
  P.close g
