(* 	$Id: ex1.ml,v 1.2 2004-07-30 19:45:32 chris_77 Exp $	 *)
(* From the Matlab plotting examples at
   http://www.indiana.edu/~statmath/math/matlab/plotting/
*)

module P = Gnuplot
open Parse_args

let red = 0xFF0000
let green = 0x00AA00
let blue = 0x0000FF

let () =
  let g = P.init ?offline:(offline 1) ~xsize:500. ~ysize:300. (device 1) in
  P.box g;
  P.title g "sinus";
  P.color g red;
  P.fx g sin (-10.) 10.;
  P.close g

let () =
  let a = -6.
  and b = 6. in
  let g = P.init ?offline:(offline 2) (device 2) in
  P.env g a b (-2.) 2.;
  P.color g red;
  P.fx g cos a b;
  P.color g green;
  P.fx g (fun x -> 1. -. x**2. /. 2.) a b;
  P.color g blue;
  P.fx g (fun x -> 1. -. x**2. /. 2. +. x**4. /. 24.) a b;
  P.close g

let () =
  let g = P.init ?offline:(offline 3) (device 3) in
  P.box g;
  P.color g blue;
  P.xy_param g (fun t -> (t *. cos t, t *. sin t)) 0. 10.;
  P.close g
