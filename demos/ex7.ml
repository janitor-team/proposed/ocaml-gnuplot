(* 	$Id: ex7.ml,v 1.3 2004-11-22 19:54:26 chris_77 Exp $	 *)
(* Test the plotting of an empty data set but with a box. *)

module P = Gnuplot
open Parse_args

let () =
  let g = P.init ?offline:(offline 1) (device 1) in
  P.env g ~xgrid:true (-2.) 2. (-1.) 1.;
  try P.close g
  with Failure _ -> print_endline "*** Gnuplot had a problem! ***"
