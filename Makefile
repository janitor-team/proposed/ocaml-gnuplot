# Ocaml-Gnuplot
# Copyright (C) Christophe TROESTLER

PKGNAME		= $(shell grep name META.in | \
			sed -e "s/.*\"\([^\"]*\)\".*/\1/")
PKGVERSION	= $(shell grep "@version" gnuplot.mli | \
			sed -r -e "s/[^0-9.]*([0-9.]+).*/\1/")

OCAMLOPT_EXISTS = $(shell which ocamlopt)

include Makefile.conf

OCAMLFLAGS 	= -dtypes
OCAMLOPTFLAGS	= -inline 2
OCAMLDOCFLAGS	= -stars -colorize-code
SED		= sed

# Order of the sources _must_ reflect order of dependency
GENERATED_SOURCES0 = gnuplot_sys.ml
GENERATED_SOURCES1 = gnuplot_ba_c.ml gnuplot_ba_f.ml gnuplot_array.ml
SOURCES = gnuplot_dir.ml $(GENERATED_SOURCES0) gnuplot_common_.ml \
  $(GENERATED_SOURCES1) gnuplot_bigarray.ml gnuplot_functor.ml gnuplot.ml
INTERFACES = gnuplot_common.mli gnuplot.mli

DISTFILES	= AUTHORS INSTALL LICENSE META.in Makefile Makefile.conf \
  $(filter-out $(GENERATED_SOURCES0),$(SOURCES)) $(INTERFACES) demos/ \
  gnuplot_arr_ba.ml gnuplot_macos.ml gnuplot_unix.ml gnuplot_win32.ml \
  make_array.ml gnuplot_dir.mli gnuplot_sys.mli

INSTALL_FILES = gnuplot_common.mli gnuplot.mli gnuplot_common.cmi gnuplot.cmi \
  gnuplot.cma
ifeq ($(OCAMLOPT_EXISTS),)
else
INSTALL_FILES += gnuplot.a gnuplot.cmx gnuplot.cmxa
endif

PKG_TARBALL 	= ocaml-$(PKGNAME)-$(PKGVERSION).tar.gz
ARCHIVE 	= $(shell grep "archive(byte)" META.in | \
			sed -e "s/.*\"\([^\"]*\)\".*/\1/")
XARCHIVE 	= $(shell grep "archive(native)" META.in | \
			sed -e "s/.*\"\([^\"]*\)\".*/\1/")

SRC_WEB 	= web
SF_WEB  	= /home/groups/o/oc/ocaml-gnuplot/htdocs/


default: all

.PHONY: all byte opt install install-byte install-opt doc dist
all: byte opt
byte: $(ARCHIVE)
opt: $(XARCHIVE)
doc: html

gnuplot.cma: $(INTERFACES:.mli=.cmi) $(SOURCES:.ml=.cmo)
	$(OCAMLC) $(OCAMLFLAGS) -a -o $@  $(SOURCES:.ml=.cmo)
gnuplot.cmxa: $(INTERFACES:.mli=.cmi) $(SOURCES:.ml=.cmx)
	$(OCAMLOPT) $(OCAMLOPTFLAGS) -a -o $@  $(SOURCES:.ml=.cmx)

# Generated files

gnuplot_array.ml: make_array.exe gnuplot_arr_ba.ml
	./$<
gnuplot_ba_%.ml: make_array.exe gnuplot_arr_ba.ml
	./$<

make_array.exe: make_array.ml
	$(OCAMLC) $(OCAMLFLAGS) -o $@ unix.cma str.cma $<

gnuplot_sys.ml: gnuplot_$(PLATFORM).ml
	$(COPY) $< $@

# Installation
.PHONY: install
install: META all
	$(OCAMLFIND) install $(PKGNAME) META $(INSTALL_FILES)

uninstall:
	$(OCAMLFIND) remove $(PKGNAME)

META: META.in
	$(COPY) $^ $@
	echo "version = \"$(PKGVERSION)\"" >> $@

# Packaging
.PHONY: tar
tar: $(PKG_TARBALL)

TARBALL_DIR=$(subst .tar.gz,,$(PKG_TARBALL))
$(PKG_TARBALL): $(DISTFILES)
	mkdir $(TARBALL_DIR)
	cp --preserve -r $(DISTFILES) $(TARBALL_DIR)/
	tar --exclude ".bzrignore" --exclude-from=.bzrignore \
	  -zcvf $(PKG_TARBALL) $(TARBALL_DIR)
	rm -rf $(TARBALL_DIR)

.PHONY: sync-svn
sync-svn:
	bzr push https://$$SF_USER@ocaml-gnuplot.svn.sourceforge.net/svnroot/ocaml-gnuplot/trunk/ocaml-gnuplot

# Documentation
html: $(PKGNAME).html/index.html

$(PKGNAME).html/index.html: $(INTERFACES) $(INTERFACES:.mli=.cmi)
	[ -d $(PKGNAME).html/ ] || mkdir $(PKGNAME).html
	$(OCAMLDOC) -d $(PKGNAME).html -html $(OCAMLDOCFLAGS) $(INTERFACES)
# 	cd $(PKGNAME).html; \
# 	for f in *; do \
# 		$(COPY) $$f $$f.bak; \
# 		$(SED) -e 's/$$VERSION/$(PKGVERSION)/' < $$f.bak > $$f; \
# 		rm -f $$f.bak; \
# 	done


# Examples

.PHONY: demos demos.opt webdemos
demos: byte
	cd demos/; $(MAKE)
demos.opt: opt
	cd demos/; $(MAKE) opt
webdemos: demos
	mkdir images/
	cd demos/; \
	for f in ex*.exe; do ./$$f -dev png; done; \
	mv *.png ../images/
#	Upload the demos to SF
	scp -r images/ shell.sf.net:$(SF_WEB)
	rm -rf images


# Release a Sourceforge tarball and publish the HTML doc 
.PHONY: web upload
web: doc
	@ if [ -d $(PKGNAME).html ] ; then \
	  scp -r $(PKGNAME).html shell.sf.net:$(SF_WEB)/ \
	  && echo "*** Published documentation on SF" ; \
	fi
	@ if [ -d $(SRC_WEB)/ ] ; then \
	  scp $(SRC_WEB)/*.html $(SRC_WEB)/*.jpg LICENSE \
	    shell.sf.net:$(SF_WEB) \
	  && echo "*** Published web site ($(SRC_WEB)/) on SF" ; \
	fi

upload: $(PKG_TARBALL)
	@ if [ -z "$(PKG_TARBALL)" ]; then \
		echo "PKG_TARBALL not defined"; exit 1; fi
	scp -C $(PKG_TARBALL) $$SF_USER@frs.sourceforge.net:uploads

# Caml general dependencies
.SUFFIXES: .ml .mli .cmi .cmo .cma .cmx .cmxa
%.cmi: %.mli
	$(OCAMLC) $(OCAMLFLAGS) -c $<
%.cmo: %.ml
	$(OCAMLC) $(OCAMLFLAGS) -c $<
%.cma: %.cmo
	$(OCAMLC) $(OCAMLFLAGS) -a -o $@  $^
%.cmx: %.ml
	$(OCAMLOPT) $(OCAMLOPTFLAGS) -c $<
%.cmxa: %.cmx
	$(OCAMLOPT) $(OCAMLOPTFLAGS) -a -o $@  $^

.PHONY: depend
depend: .depend
.depend: $(SOURCES) $(wildcard *.mli)
	$(OCAMLDEP) $^ > .depend

include .depend

########################################################################

.PHONY: clean dist-clean
clean:
	@ rm -f *~ .*~ *.{o,a} *.cm[aiox] *.cmxa *.annot
	@ rm -f $(GENERATED_SOURCES0) $(GENERATED_SOURCES1)
	@ rm -rf $(PKGNAME).html $(PKG_TARBALL)
	@ find . -type f -perm -u=x -exec rm -f {} \;
	@ if [ -d ./html ]; then rm -rf ./html; fi
	cd demos; $(MAKE) clean

dist-clean: clean
	rm -f .depend META
