(* File: gnuplot_functor.ml

   Copyright (C) 2001-2004

     Christophe Troestler
     email: Christophe.Troestler@umh.ac.be
     WWW: http://math.umh.ac.be/an/software/

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public License
   version 2.1 as published by the Free Software Foundation, with the
   special exception on linking described in file LICENSE.

   This library is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the file
   LICENSE for more details.
*)
(* 	$Id: gnuplot_functor.ml,v 1.6 2004-11-28 23:49:39 chris_77 Exp $	 *)

open Printf
open Bigarray

(***********************************************************************
 *
 *                        FUNCTORIAL INTERFACE
 *
 ***********************************************************************)


module type DATA =
sig
  type vec
  val iter : (float -> unit) -> vec -> unit

  type vec2
  val iter2 : (float -> float -> unit) -> vec2 -> unit

  type vec4
  val iter4 : (float -> float -> float -> float -> unit) -> vec4 -> unit

  type mat
  val iter_mat : (float -> float -> float -> unit) ->
    vec -> vec -> mat -> unit
end

module type PLOT_DATA =
sig
  type vec
  type vec2
  type vec4
  type mat

  include Gnuplot_common.T

  val x : handle -> ?tag:int -> ?style:style -> ?label:string -> ?n0:int ->
    vec -> unit
  val xy : handle -> ?tag:int -> ?style:style -> ?label:string -> vec2 -> unit
  val bin : handle -> ?tag:int -> ?label:string -> vec2 -> unit
  val vector : handle -> ?tag:int -> ?label:string -> vec4 -> unit
  val err : handle -> ?tag:int -> vec4 -> unit
  val xyz :
    handle -> ?tag:int -> ?style:style -> ?label:string -> vec -> vec -> mat -> unit
end


module Make(D : DATA) =
struct
  type vec = D.vec
  type vec2 = D.vec2
  type vec4 = D.vec4
  type mat = D.mat

  include Gnuplot_common_

  let data_of_float x =
    if is_finite x then string_of_float x else "?"

  let x g ?(tag=system_tag) ?(style=Lines) ?(label="") ?(n0=0) xvec =
    if g.closed then failwith "Gnuplot.Make().x";
    let save_data out =
      let n = ref n0 in
      D.iter (fun x ->
                out (string_of_int !n ^ " " ^ data_of_float x ^ "\n");
                incr n) xvec  in
    let nlines = 0 (* FIXME *)in
    plot_data g ~inline_if:(nlines <= g.max_inline)
      save_data ~axes ~label ~style:(with_style g style) ~tag


  let xy g ?(tag=system_tag) ?(style=Lines) ?(label="") vec2 =
    if g.closed then failwith "Gnuplot.Make().xy";
    let save_data out =
      D.iter2 (fun x y ->
                 out (data_of_float x ^ " " ^ data_of_float y ^ "\n")) vec2 in
    let nlines = 0 (* FIXME *)in
    plot_data g ~inline_if:(nlines <= g.max_inline)
      save_data ~axes ~label ~style:(with_style g style) ~tag


  let bin g ?(tag=system_tag) ?(label="") vec2 =
    if g.closed then failwith "Gnuplot.Make().bin";
    let box out xlow xup y =
      if is_finite xlow && is_finite xup && is_finite y then
        let b0 = string_of_float xlow
        and b1 = string_of_float xup
        and y  = string_of_float y in
        out(sprintf "%s 0.\n%s %s\n%s %s\n%s 0.\n" b0 b0 y b1 y b1)
      else out "? ?\n" in
    let save_data out =
      let first_data = ref true
      and second_data = ref true
      and x_1 = ref neg_infinity
      and x0 = ref neg_infinity
      and y0 = ref 0.  in
      let make_box x y =
        if !first_data then (first_data := false; x_1 := x; y0 := y)
        else if !second_data then begin
          second_data := false;
          box out (0.5 *. (3. *. !x_1 -. x)) (0.5 *. (!x_1 +. x)) !y0;
          x0 := x; y0 := y
        end
        else begin
          box out (0.5 *. (!x_1 +. !x0)) (0.5 *. (!x0 +. x)) !y0;
          x_1 := !x0;  x0 := x;  y0 := y;
        end in
      D.iter2 make_box vec2;
      (* Last box *)
      box out (0.5 *. (!x_1 +. !x0)) (0.5 *. (3. *. !x0 -. !x_1)) !y0 in
    let nbins = 0 (* FIXME *)in
    let filledcurves = (* FIXME: active axes *)
      "with filledcurves y1 = 0 lw "
      ^ string_of_float g.pen_width ^ " lt " in
    plot_data g ~inline_if:(4 * nbins <= g.max_inline)
      save_data ~axes ~label ~style:filledcurves ~tag


  let vector g ?(tag=system_tag) ?(label="") vec4 =
    if g.closed then failwith "Gnuplot.Make().vector";
    let save_data out =
      D.iter4
        (fun x y dx dy ->
           if is_finite x && is_finite y && is_finite dx && is_finite dy then
             out (sprintf "%F %F %F %F\n" x y dx dy)
           else out "? ? ? ?\n"
        ) vec4 in
    let nlines = 0 (* FIXME *)in
    plot_data g ~inline_if:(nlines <= g.max_inline)
      save_data ~axes ~label ~style:with_vector ~tag



  let err g ?(tag=system_tag) vec4 =
    if g.closed then failwith "Gnuplot.Make().err";
    failwith "Make().err not yet implemented"

  let xyz g ?(tag=system_tag) ?(style=Lines) ?(label="") =
    if g.closed then failwith "Gnuplot.Make().xyz";
    failwith "Make().xyz not yet implemented"
end
